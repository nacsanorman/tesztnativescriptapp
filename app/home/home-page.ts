import { EventData } from "tns-core-modules/data/observable";
import { Page, View } from "tns-core-modules/ui/page";
import { SwipeActionsEventData } from "nativescript-ui-listview";

import { HomeViewModel } from "./home-view-model";

export function navigatingTo(args: EventData) {
    const page = <Page>args.object;

    page.bindingContext = new HomeViewModel();
}

export function onSwipeCellStarted(args: SwipeActionsEventData) {
    const swipeLimits = args.data.swipeLimits;
    swipeLimits.left = 360;
    swipeLimits.right = 0;
    swipeLimits.threshold = 200;
}
