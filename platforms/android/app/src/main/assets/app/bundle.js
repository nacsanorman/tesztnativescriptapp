require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app-root.xml": "./app-root.xml",
	"./app.css": "./app.css",
	"./app.js": "./app.js",
	"./app.ts": "./app.ts",
	"./home/data.ts": "./home/data.ts",
	"./home/home-page.css": "./home/home-page.css",
	"./home/home-page.js": "./home/home-page.js",
	"./home/home-page.ts": "./home/home-page.ts",
	"./home/home-page.xml": "./home/home-page.xml",
	"./home/home-view-model.js": "./home/home-view-model.js",
	"./home/home-view-model.ts": "./home/home-view-model.ts",
	"./observable-property-decorator.js": "./observable-property-decorator.js",
	"./observable-property-decorator.ts": "./observable-property-decorator.ts"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$";

/***/ }),

/***/ "./app-root.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Frame defaultPage=\"home/home-page\"></Frame>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app-root.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./app-root.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("~@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("~@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));
global.registerModule("@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"import","import":"'~@nativescript/theme/css/core.css'"},{"type":"import","import":"'~@nativescript/theme/css/default.css'"},{"type":"rule","selectors":[".btn"],"declarations":[{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".home-panel"],"declarations":[{"type":"declaration","property":"vertical-align","value":"center"},{"type":"declaration","property":"font-size","value":"20"},{"type":"declaration","property":"margin","value":"15"}]},{"type":"rule","selectors":[".description-label"],"declarations":[{"type":"declaration","property":"margin-bottom","value":"15"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./app.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var app = __webpack_require__("tns-core-modules/application");
app.run({ moduleName: 'app-root' });
/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/


/***/ }),

/***/ "./app.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("tns-core-modules/application");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_0__);

        let applicationCheckPlatform = __webpack_require__("tns-core-modules/application");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("tns-core-modules/ui/frame");
__webpack_require__("tns-core-modules/ui/frame/activity");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__coreModulesLiveSync = global.__onLiveSync;

            global.__onLiveSync = function () {
                // handle hot updated on LiveSync
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                // the hot updates are applied, ask the modules to apply the changes
                setTimeout(() => {
                    global.__coreModulesLiveSync({ type, path });
                });
            };

            // handle hot updated on initial app start
            hmrUpdate();
        }
        
            const context = __webpack_require__("./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$");
            global.registerWebpackModules(context);
            if (true) {
                module.hot.accept(context.id, () => { 
                    console.log("HMR: Accept module '" + context.id + "' from '" + module.i + "'"); 
                });
            }
            
        __webpack_require__("tns-core-modules/bundle-entry-points");
        
tns_core_modules_application__WEBPACK_IMPORTED_MODULE_0__["run"]({ moduleName: 'app-root' });
/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.ts") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./app.ts" });
    });
} 
    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./home/data.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getData", function() { return getData; });
function getData() {
    return new Promise(function (resolve, reject) {
        var dataCopy = JSON.parse(JSON.stringify(data));
        for (var i = 0; i < data.length; i++) {
            dataCopy[i].favourite = false;
            var randomImageId = Math.floor(Math.random() * 7);
            dataCopy[i].image = "~/images/grocery" + randomImageId + ".jpg";
        }
        setTimeout(function () {
            resolve(dataCopy);
        }, 1000);
    });
}
var data = [
    { "id": 1, "product": "Tart Shells - Sweet, 2", "price": "$2.46", "available": true },
    { "id": 2, "product": "Chocolate Eclairs", "price": "$8.20", "available": true },
    { "id": 3, "product": "Oil - Shortening - All - Purpose", "price": "$5.22", "available": true },
    { "id": 4, "product": "Lid - 10,12,16 Oz", "price": "$2.82", "available": true },
    { "id": 5, "product": "Duck - Fat", "price": "$7.47", "available": false },
    { "id": 6, "product": "Chocolate Bar - Reese Pieces", "price": "$5.87", "available": false },
    { "id": 7, "product": "Muffin Orange Individual", "price": "$2.93", "available": true },
    { "id": 8, "product": "Pastry - Cherry Danish - Mini", "price": "$9.32", "available": true },
    { "id": 9, "product": "Wine - Maipo Valle Cabernet", "price": "$8.62", "available": true },
    { "id": 10, "product": "Juice - Orangina", "price": "$2.74", "available": true },
    { "id": 11, "product": "Meldea Green Tea Liquor", "price": "$9.01", "available": false },
    { "id": 12, "product": "Beef Flat Iron Steak", "price": "$2.41", "available": true },
    { "id": 13, "product": "Foam Espresso Cup Plain White", "price": "$6.82", "available": false },
    { "id": 14, "product": "Yeast - Fresh, Fleischman", "price": "$6.18", "available": false },
    { "id": 15, "product": "Okra", "price": "$6.56", "available": true },
    { "id": 16, "product": "Oysters - Smoked", "price": "$2.59", "available": false },
    { "id": 17, "product": "Coffee Caramel Biscotti", "price": "$2.14", "available": true },
    { "id": 18, "product": "Savory", "price": "$3.15", "available": false },
    { "id": 19, "product": "Veal - Inside", "price": "$4.75", "available": false },
    { "id": 20, "product": "Wine - Red, Gallo, Merlot", "price": "$1.26", "available": true },
    { "id": 21, "product": "Oil - Macadamia", "price": "$8.61", "available": true },
    { "id": 22, "product": "Spring Roll Wrappers", "price": "$0.62", "available": false },
    { "id": 23, "product": "Shortbread - Cookie Crumbs", "price": "$4.11", "available": true },
    { "id": 24, "product": "Puff Pastry - Sheets", "price": "$5.58", "available": true },
    { "id": 25, "product": "Appetizer - Assorted Box", "price": "$6.90", "available": true },
    { "id": 26, "product": "Onions - Red Pearl", "price": "$5.59", "available": true },
    { "id": 27, "product": "Cheese - Brick With Onion", "price": "$8.21", "available": true },
    { "id": 28, "product": "Beer - Sleeman Fine Porter", "price": "$3.63", "available": false },
    { "id": 29, "product": "Stock - Beef, White", "price": "$9.06", "available": true },
    { "id": 30, "product": "Salt - Table", "price": "$6.29", "available": true },
    { "id": 31, "product": "Sugar - Sweet N Low, Individual", "price": "$9.08", "available": false },
    { "id": 32, "product": "Wine - George Duboeuf Rose", "price": "$5.88", "available": true },
    { "id": 33, "product": "Bread - Flat Bread", "price": "$1.64", "available": true },
    { "id": 34, "product": "Lamb - Shoulder", "price": "$0.24", "available": true },
    { "id": 35, "product": "Vaccum Bag 10x13", "price": "$10.00", "available": false },
    { "id": 36, "product": "Milk Powder", "price": "$9.30", "available": false },
    { "id": 37, "product": "Trout Rainbow Whole", "price": "$1.94", "available": true },
    { "id": 38, "product": "Ham - Cooked", "price": "$4.11", "available": false },
    { "id": 39, "product": "Straws - Cocktale", "price": "$6.75", "available": false },
    { "id": 40, "product": "Rosemary - Fresh", "price": "$9.03", "available": true },
    { "id": 41, "product": "Lid - 10,12,16 Oz", "price": "$6.60", "available": false },
    { "id": 42, "product": "Muffin Mix - Morning Glory", "price": "$0.30", "available": true },
    { "id": 43, "product": "Fish - Soup Base, Bouillon", "price": "$1.04", "available": true },
    { "id": 44, "product": "Beer - Maudite", "price": "$4.56", "available": false },
    { "id": 45, "product": "Parasol Pick Stir Stick", "price": "$2.42", "available": true },
    { "id": 46, "product": "Pickerel - Fillets", "price": "$5.29", "available": false },
    { "id": 47, "product": "Bagel - Everything Presliced", "price": "$9.35", "available": false },
    { "id": 48, "product": "Pork - Butt, Boneless", "price": "$1.52", "available": false },
    { "id": 49, "product": "Vector Energy Bar", "price": "$1.26", "available": false },
    { "id": 50, "product": "Veal - Sweetbread", "price": "$7.00", "available": true },
    { "id": 51, "product": "Lamb - Whole Head Off,nz", "price": "$7.99", "available": true },
    { "id": 52, "product": "Arctic Char - Fresh, Whole", "price": "$1.09", "available": false },
    { "id": 53, "product": "Muffin Hinge - 211n", "price": "$8.31", "available": true },
    { "id": 54, "product": "Pears - Anjou", "price": "$7.22", "available": true },
    { "id": 55, "product": "Tamarind Paste", "price": "$9.48", "available": false },
    { "id": 56, "product": "Cheese - Swiss Sliced", "price": "$2.52", "available": true },
    { "id": 57, "product": "Fib N9 - Prague Powder", "price": "$2.72", "available": true },
    { "id": 58, "product": "Cup - Paper 10oz 92959", "price": "$3.36", "available": false },
    { "id": 59, "product": "Wine - Lamancha Do Crianza", "price": "$7.10", "available": false },
    { "id": 60, "product": "Isomalt", "price": "$6.76", "available": true },
    { "id": 61, "product": "Haggis", "price": "$2.91", "available": true },
    { "id": 62, "product": "Icecream - Dstk Strw Chseck", "price": "$5.15", "available": false },
    { "id": 63, "product": "Bread - White, Sliced", "price": "$9.47", "available": false },
    { "id": 64, "product": "Tea - Black Currant", "price": "$5.16", "available": false },
    { "id": 65, "product": "Truffle Cups - Brown", "price": "$8.77", "available": true },
    { "id": 66, "product": "Pepper - Julienne, Frozen", "price": "$9.40", "available": false },
    { "id": 67, "product": "Flower - Potmums", "price": "$8.68", "available": false },
    { "id": 68, "product": "Lamb Leg - Bone - In Nz", "price": "$0.14", "available": false },
    { "id": 69, "product": "Kellogs All Bran Bars", "price": "$9.42", "available": false },
    { "id": 70, "product": "Cheese - Cambozola", "price": "$8.30", "available": false },
    { "id": 71, "product": "Pepper - Jalapeno", "price": "$9.38", "available": false },
    { "id": 72, "product": "Glycerine", "price": "$7.47", "available": true },
    { "id": 73, "product": "Muffin Mix - Chocolate Chip", "price": "$2.93", "available": true },
    { "id": 74, "product": "Juice - Apple Cider", "price": "$9.44", "available": true },
    { "id": 75, "product": "Apple - Macintosh", "price": "$1.85", "available": true },
    { "id": 76, "product": "Prunes - Pitted", "price": "$6.96", "available": false },
    { "id": 77, "product": "Pineapple - Regular", "price": "$7.36", "available": true },
    { "id": 78, "product": "Cheese - Swiss", "price": "$0.57", "available": true },
    { "id": 79, "product": "Lettuce - Green Leaf", "price": "$8.26", "available": true },
    { "id": 80, "product": "Wine - Dubouef Macon - Villages", "price": "$8.47", "available": true },
    { "id": 81, "product": "Pasta - Cannelloni, Sheets, Fresh", "price": "$7.66", "available": true },
    { "id": 82, "product": "Cheese - Swiss Sliced", "price": "$8.49", "available": false },
    { "id": 83, "product": "Salmon Steak - Cohoe 8 Oz", "price": "$8.14", "available": false },
    { "id": 84, "product": "Wine - Tribal Sauvignon", "price": "$4.96", "available": true },
    { "id": 85, "product": "Turkey Leg With Drum And Thigh", "price": "$5.33", "available": true },
    { "id": 86, "product": "Shallots", "price": "$8.71", "available": false },
    { "id": 87, "product": "Mushroom - Shitake, Dry", "price": "$8.58", "available": false },
    { "id": 88, "product": "Pie Pecan", "price": "$4.72", "available": true },
    { "id": 89, "product": "Noodles - Steamed Chow Mein", "price": "$6.05", "available": false },
    { "id": 90, "product": "Trueblue - Blueberry Cranberry", "price": "$3.85", "available": false },
    { "id": 91, "product": "Turnip - Mini", "price": "$2.65", "available": true },
    { "id": 92, "product": "V8 Splash Strawberry Kiwi", "price": "$7.65", "available": true },
    { "id": 93, "product": "Pears - Anjou", "price": "$3.10", "available": false },
    { "id": 94, "product": "Wine - White, Riesling, Henry Of", "price": "$5.89", "available": true },
    { "id": 95, "product": "Bread Ww Cluster", "price": "$9.39", "available": true },
    { "id": 96, "product": "Bread - Italian Sesame Poly", "price": "$6.61", "available": false },
    { "id": 97, "product": "Duck - Legs", "price": "$1.59", "available": false },
    { "id": 98, "product": "French Pastry - Mini Chocolate", "price": "$7.83", "available": true },
    { "id": 99, "product": "Napkin - Beverage 1 Ply", "price": "$3.44", "available": true },
    { "id": 100, "product": "Island Oasis - Peach Daiquiri", "price": "$1.31", "available": false }
];
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./home/data.ts") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./home/data.ts" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./home/home-page.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".action-bar"],"declarations":[{"type":"declaration","property":"background-color","value":"#0E4375"},{"type":"declaration","property":"color","value":"#F5C851"}]},{"type":"rule","selectors":[".list-group",".list-item"],"declarations":[{"type":"declaration","property":"background-color","value":"#FFFFFF"}]},{"type":"rule","selectors":[".header-linear"],"declarations":[{"type":"declaration","property":"background-color","value":"#5BBC93"}]},{"type":"rule","selectors":[".list-item-linear"],"declarations":[{"type":"declaration","property":"border-bottom-width","value":"1"},{"type":"declaration","property":"border-color","value":"#0E4375"}]},{"type":"rule","selectors":[".title-linear"],"declarations":[{"type":"declaration","property":"color","value":"#0E4375"},{"type":"declaration","property":"padding","value":"0 0 0 10"}]},{"type":"rule","selectors":[".price-linear"],"declarations":[{"type":"declaration","property":"color","value":"#EF823F"},{"type":"declaration","property":"padding","value":"0 0 0 10"}]},{"type":"rule","selectors":[".swipe-linear"],"declarations":[{"type":"declaration","property":"background-color","value":"#EF823F"}]},{"type":"rule","selectors":[".fav-linear"],"declarations":[{"type":"declaration","property":"margin","value":"30"}]},{"type":"rule","selectors":[".list-item-grid"],"declarations":[{"type":"declaration","property":"margin","value":"1"}]},{"type":"rule","selectors":[".list-item-grid-background"],"declarations":[{"type":"declaration","property":"background-color","value":"#000000"},{"type":"declaration","property":"opacity","value":"0.6"}]},{"type":"rule","selectors":[".title-grid"],"declarations":[{"type":"declaration","property":"color","value":"#FFFFFF"},{"type":"declaration","property":"padding","value":"0 0 0 10"}]},{"type":"rule","selectors":[".price-grid"],"declarations":[{"type":"declaration","property":"color","value":"#EF823F"},{"type":"declaration","property":"padding","value":"0 0 5 10"}]},{"type":"rule","selectors":[".fav-grid"],"declarations":[{"type":"declaration","property":"margin","value":"0 10 5 0"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./home/home-page.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./home/home-page.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./home/home-page.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var home_view_model_1 = __webpack_require__("./home/home-view-model.ts");
function pageLoaded(args) {
    var page = args.object;
    page.bindingContext = new home_view_model_1.HomeViewModel();
}
exports.pageLoaded = pageLoaded;


/***/ }),

/***/ "./home/home-page.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navigatingTo", function() { return navigatingTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onSwipeCellStarted", function() { return onSwipeCellStarted; });
/* harmony import */ var _home_view_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./home/home-view-model.ts");

function navigatingTo(args) {
    var page = args.object;
    page.bindingContext = new _home_view_model__WEBPACK_IMPORTED_MODULE_0__["HomeViewModel"]();
}
function onSwipeCellStarted(args) {
    var swipeLimits = args.data.swipeLimits;
    swipeLimits.left = 360;
    swipeLimits.right = 0;
    swipeLimits.threshold = 200;
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./home/home-page.ts") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./home/home-page.ts" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./home/home-page.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("nativescript-ui-listview"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("nativescript-ui-listview"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplates", function() { return __webpack_require__("nativescript-ui-listview"); });
global.registerModule("nativescript-ui-listview/RadListView.itemSwipeTemplate", function() { return __webpack_require__("nativescript-ui-listview"); });

module.exports = "<Page navigatingTo=\"navigatingTo\" class=\"page\" xmlns=\"http://www.nativescript.org/tns.xsd\"\n    xmlns:lv=\"nativescript-ui-listview\">\n\n    <ActionBar class=\"action-bar\">\n        <GridLayout class=\"action-bar-title\" width=\"100%\">\n            <Label text=\"Storefront\"></Label>\n            <StackLayout orientation=\"horizontal\" horizontalAlignment=\"right\"\n                margin=\"0 15 0 15\">\n                <Image src=\"~/images/fav-outline.png\" height=\"30\" tap=\"{{ toggleFavouritesFilter }}\"\n                    margin=\"0 5 0 5\"></Image>\n                <Image src=\"~/images/layout-grid.png\" height=\"30\" tap=\"{{ changeLayout }}\"\n                    margin=\"0 5 0 5\"></Image>\n            </StackLayout>\n        </GridLayout>\n    </ActionBar>\n\n    <GridLayout>\n        <lv:RadListView id=\"list-view\" class=\"list-group\" items=\"{{ dataItems }}\"\n            loaded=\"{{ onRadListViewLoaded }}\" swipeActions=\"true\"\n            itemTemplateSelector=\"{{ selectItemTemplate }}\" separatorColor=\"transparent\"\n            selectionBehavior=\"None\" itemSwipeProgressStarted=\"onSwipeCellStarted\"\n            pullToRefresh=\"true\" pullToRefreshInitiated=\"{{ onPullToRefreshInitiated }}\">\n\n            <lv:RadListView.itemTemplates>\n                <template key=\"linear\">\n                    <GridLayout rows=\"100\" columns=\"120, *, 80\" class=\"list-item list-item-linear\">\n                        <Image col=\"0\" rowSpan=\"2\" src=\"{{ image }}\" stretch=\"aspectFit\" />\n                        <Label col=\"1\" text=\"{{ product }}\" class=\"h3 title-linear\"\n                            textWrap=\"true\" verticalAlignment=\"middle\" />\n                        <Label col=\"2\" text=\"{{ price }}\" class=\"h2 price-linear\"\n                            textWrap=\"true\" verticalAlignment=\"middle\" />\n                    </GridLayout>\n                </template>\n\n                <template key=\"grid\">\n                    <GridLayout rows=\"100, 60\" class=\"list-item list-item-grid\">\n                        <Image row=\"0\" src=\"{{ image }}\" stretch=\"aspectFill\"\n                            rowSpan=\"3\" />\n                        <GridLayout row=\"1\" rows=\"25, 35\" columns=\"*, *\"\n                            class=\"list-item-grid-background\">\n                            <Label row=\"0\" col=\"0\" colSpan=\"2\" text=\"{{ product }}\"\n                                class=\"h3 title-grid\" />\n                            <Label row=\"1\" col=\"0\" text=\"{{ price }}\" class=\"h2 price-grid\" />\n                            <Image row=\"1\" col=\"1\" src=\"{{ favourite ? '~/images/fav-solid.png' : '~/images/fav-outline.png'}}\"\n                                class=\"fav-grid\" tap=\"{{ toggleFavourite }}\"\n                                horizontalAlignment=\"right\" />\n                        </GridLayout>\n                    </GridLayout>\n                </template>\n            </lv:RadListView.itemTemplates>\n\n            <lv:RadListView.itemSwipeTemplate>\n                <GridLayout class=\"swipe-linear\">\n                    <Image src=\"{{ favourite ? '~/images/fav-solid.png' : '~/images/fav-outline.png'}}\"\n                        class=\"fav-linear\" tap=\"{{ toggleFavourite }}\"\n                        horizontalAlignment=\"left\" margin=\"30\"></Image>\n                </GridLayout>\n            </lv:RadListView.itemSwipeTemplate>\n        </lv:RadListView>\n\n        <ActivityIndicator busy=\"{{ isBusy }}\" class=\"activity-indicator\" />\n    </GridLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./home/home-page.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./home/home-page.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./home/home-view-model.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = __webpack_require__("tns-core-modules/data/observable");
var HomeViewModel = /** @class */ (function (_super) {
    __extends(HomeViewModel, _super);
    function HomeViewModel() {
        return _super.call(this) || this;
    }
    return HomeViewModel;
}(observable_1.Observable));
exports.HomeViewModel = HomeViewModel;


/***/ }),

/***/ "./home/home-view-model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeViewModel", function() { return HomeViewModel; });
/* harmony import */ var _observable_property_decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./observable-property-decorator.ts");
/* harmony import */ var tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("tns-core-modules/data/observable-array");
/* harmony import */ var tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_data_observable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("tns-core-modules/data/observable");
/* harmony import */ var tns_core_modules_data_observable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_data_observable__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("tns-core-modules/platform");
/* harmony import */ var tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var nativescript_ui_listview__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("nativescript-ui-listview");
/* harmony import */ var nativescript_ui_listview__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_listview__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./home/data.ts");






var headerItemTemplate = "\n    <GridLayout class=\"header-linear\">\n        <Label text=\"Swipe Right to Add to Favourites\"\n            horizontalAlignment=\"center\" />\n    </GridLayout>\n";
var isListViewLinearLayout = true;
var HomeViewModel = /** @class */ (function (_super) {
    __extends(HomeViewModel, _super);
    function HomeViewModel() {
        var _this = _super.call(this) || this;
        _this.isBusy = true;
        _this.showFavouritesFilter = false;
        _this.isBusy = true;
        _this.showFavouritesFilter = false;
        _this.dataItems = new tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1__["ObservableArray"]();
        Object(_data__WEBPACK_IMPORTED_MODULE_5__["getData"])().then(function (data) {
            _this.dataItems.push(data);
            _this.isBusy = false;
        });
        return _this;
    }
    HomeViewModel.prototype.onRadListViewLoaded = function (args) {
        var listView = args.object;
        this.toggleHeaderItemTemplate(listView);
    };
    HomeViewModel.prototype.toggleHeaderItemTemplate = function (listView) {
        if (!isListViewLinearLayout) {
            listView.headerItemTemplate = undefined;
        }
        else {
            listView.headerItemTemplate = headerItemTemplate;
        }
    };
    HomeViewModel.prototype.selectItemTemplate = function (item, index, items) {
        return isListViewLinearLayout ? "linear" : "grid";
    };
    HomeViewModel.prototype.toggleFavourite = function (args) {
        var image = args.object;
        var listView = image.page.getViewById("list-view");
        var itemData = image.bindingContext;
        if (itemData.favourite) {
            image.src = "~/images/fav-outline.png";
            itemData.favourite = false;
        }
        else {
            image.src = "~/images/fav-solid.png";
            itemData.favourite = true;
        }
        listView.notifySwipeToExecuteFinished();
    };
    HomeViewModel.prototype.toggleFavouritesFilter = function (args) {
        var image = args.object;
        var listView = image.page.getViewById("list-view");
        if (this.showFavouritesFilter) {
            listView.filteringFunction = undefined;
            image.src = "~/images/fav-outline.png";
            this.showFavouritesFilter = false;
        }
        else {
            listView.filteringFunction = function (item) {
                return item.favourite;
            };
            image.src = "~/images/fav-solid.png";
            this.showFavouritesFilter = true;
        }
    };
    HomeViewModel.prototype.changeLayout = function (args) {
        var image = args.object;
        var listView = image.page.getViewById("list-view");
        if (isListViewLinearLayout) {
            var gridLayout = new nativescript_ui_listview__WEBPACK_IMPORTED_MODULE_4__["ListViewGridLayout"]();
            tns_core_modules_platform__WEBPACK_IMPORTED_MODULE_3__["isIOS"] ? gridLayout.itemHeight = 160 : null;
            gridLayout.spanCount = 2;
            listView.swipeActions = false;
            isListViewLinearLayout = false;
            this.toggleHeaderItemTemplate(listView);
            image.src = "~/images/layout-linear.png";
            listView.listViewLayout = gridLayout;
        }
        else {
            var linearLayout = new nativescript_ui_listview__WEBPACK_IMPORTED_MODULE_4__["ListViewLinearLayout"]();
            listView.swipeActions = true;
            isListViewLinearLayout = true;
            this.toggleHeaderItemTemplate(listView);
            image.src = "~/images/layout-grid.png";
            listView.listViewLayout = linearLayout;
        }
    };
    HomeViewModel.prototype.onPullToRefreshInitiated = function (args) {
        var _this = this;
        Object(_data__WEBPACK_IMPORTED_MODULE_5__["getData"])().then(function (data) {
            _this.dataItems.splice(0);
            _this.dataItems.push(data);
            var listView = args.object;
            listView.notifyPullToRefreshFinished();
        });
    };
    var _a;
    __decorate([
        Object(_observable_property_decorator__WEBPACK_IMPORTED_MODULE_0__["ObservableProperty"])(),
        __metadata("design:type", Boolean)
    ], HomeViewModel.prototype, "isBusy", void 0);
    __decorate([
        Object(_observable_property_decorator__WEBPACK_IMPORTED_MODULE_0__["ObservableProperty"])(),
        __metadata("design:type", typeof (_a = typeof tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1__["ObservableArray"] !== "undefined" && tns_core_modules_data_observable_array__WEBPACK_IMPORTED_MODULE_1__["ObservableArray"]) === "function" ? _a : Object)
    ], HomeViewModel.prototype, "dataItems", void 0);
    return HomeViewModel;
}(tns_core_modules_data_observable__WEBPACK_IMPORTED_MODULE_2__["Observable"]));

; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./home/home-view-model.ts") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./home/home-view-model.ts" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./observable-property-decorator.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function ObservableProperty() {
    return function (target, propertyKey) {
        Object.defineProperty(target, propertyKey, {
            get: function () {
                return this["_" + propertyKey];
            },
            set: function (value) {
                if (this["_" + propertyKey] === value) {
                    return;
                }
                this["_" + propertyKey] = value;
                this.notifyPropertyChange(propertyKey, value);
            },
            enumerable: true,
            configurable: true
        });
    };
}
exports.ObservableProperty = ObservableProperty;


/***/ }),

/***/ "./observable-property-decorator.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ObservableProperty", function() { return ObservableProperty; });
function ObservableProperty() {
    return function (target, propertyKey) {
        Object.defineProperty(target, propertyKey, {
            get: function () {
                return this["_" + propertyKey];
            },
            set: function (value) {
                if (this["_" + propertyKey] === value) {
                    return;
                }
                this["_" + propertyKey] = value;
                this.notifyPropertyChange(propertyKey, value);
            },
            enumerable: true,
            configurable: true
        });
    };
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./observable-property-decorator.ts") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./observable-property-decorator.ts" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "nativescript-ui-listview":
/***/ (function(module, exports) {

module.exports = require("nativescript-ui-listview");

/***/ }),

/***/ "tns-core-modules/application":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/application");

/***/ }),

/***/ "tns-core-modules/bundle-entry-points":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/bundle-entry-points");

/***/ }),

/***/ "tns-core-modules/data/observable":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/data/observable");

/***/ }),

/***/ "tns-core-modules/data/observable-array":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/data/observable-array");

/***/ }),

/***/ "tns-core-modules/file-system":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/file-system");

/***/ }),

/***/ "tns-core-modules/platform":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/platform");

/***/ }),

/***/ "tns-core-modules/ui/frame":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/ui/frame");

/***/ }),

/***/ "tns-core-modules/ui/frame/activity":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/ui/frame/activity");

/***/ }),

/***/ "tns-core-modules/ui/styling/style-scope":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/ui/styling/style-scope");

/***/ })

},[["./app.ts","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLiBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvYXBwXFwuKGNzc3xzY3NzfGxlc3N8c2FzcykkIiwid2VicGFjazovLy9cXGJfW1xcdy1dKlxcLilzY3NzKSQiLCJ3ZWJwYWNrOi8vLy4vYXBwLXJvb3QueG1sIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLmpzIiwid2VicGFjazovLy8uL2FwcC50cyIsIndlYnBhY2s6Ly8vLi9ob21lL2RhdGEudHMiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLXBhZ2UuY3NzIiwid2VicGFjazovLy8uL2hvbWUvaG9tZS1wYWdlLmpzIiwid2VicGFjazovLy8uL2hvbWUvaG9tZS1wYWdlLnRzIiwid2VicGFjazovLy8uL2hvbWUvaG9tZS1wYWdlLnhtbCIsIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUtdmlldy1tb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUtdmlldy1tb2RlbC50cyIsIndlYnBhY2s6Ly8vLi9vYnNlcnZhYmxlLXByb3BlcnR5LWRlY29yYXRvci5qcyIsIndlYnBhY2s6Ly8vLi9vYnNlcnZhYmxlLXByb3BlcnR5LWRlY29yYXRvci50cyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwidG5zLWNvcmUtbW9kdWxlcy9idW5kbGUtZW50cnktcG9pbnRzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZS1hcnJheVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInRucy1jb3JlLW1vZHVsZXMvZmlsZS1zeXN0ZW1cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwidG5zLWNvcmUtbW9kdWxlcy91aS9mcmFtZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInRucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWUvYWN0aXZpdHlcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3N0eWxpbmcvc3R5bGUtc2NvcGVcIiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUU7Ozs7Ozs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlKOzs7Ozs7OztBQ2xDQSxvRTtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHlDQUF5QztBQUNwRSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNSQSwrR0FBaUUsbUJBQU8sQ0FBQyxrREFBa0M7QUFDM0csZ0VBQWdFLG1CQUFPLENBQUMsa0RBQWtDO0FBQzFHLG9FQUFvRSxtQkFBTyxDQUFDLHFEQUFxQztBQUNqSCxtRUFBbUUsbUJBQU8sQ0FBQyxxREFBcUMsR0FBRyxrQkFBa0Isa0NBQWtDLFVBQVUsK0RBQStELEVBQUUsa0VBQWtFLEVBQUUsb0RBQW9ELHlEQUF5RCxFQUFFLEVBQUUsMkRBQTJELGtFQUFrRSxFQUFFLHlEQUF5RCxFQUFFLHNEQUFzRCxFQUFFLEVBQUUsa0VBQWtFLDZEQUE2RCxFQUFFLHdCO0FBQzV4QixJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixtQ0FBbUM7QUFDOUQsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ1ZhO0FBQ2IsOENBQThDLGNBQWM7QUFDNUQsVUFBVSxtQkFBTyxDQUFDLDhCQUE4QjtBQUNoRCxTQUFTLHlCQUF5QjtBQUNsQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FDUG9EO0FBQUE7QUFBQTtBQUFBO0FBQ3BELEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztBQUNwQzs7O0VBR0U7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMRjtBQUFBO0FBQU8sU0FBUyxPQUFPO0lBQ25CLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtRQUMvQixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUVsRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixRQUFRLENBQUMsQ0FBQyxDQUFFLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUVyQyxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUM5QyxRQUFRLENBQUMsQ0FBQyxDQUFFLENBQUMsS0FBSyxHQUFHLGtCQUFrQixHQUFHLGFBQWEsR0FBRyxNQUFNLENBQUM7U0FDMUU7UUFFRCxVQUFVLENBQUM7WUFDUCxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDO0FBRUQsSUFBTSxJQUFJLEdBQUc7SUFDVCxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLHdCQUF3QixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNyRixFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNoRixFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLGtDQUFrQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUMvRixFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNoRixFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDMUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDNUYsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSwwQkFBMEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDdkYsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSwrQkFBK0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDNUYsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSw2QkFBNkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDaEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDeEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDcEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwrQkFBK0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDOUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3BFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ2pGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUseUJBQXlCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3ZGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUN2RSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDOUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDekYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDL0UsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDckYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw0QkFBNEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDcEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwwQkFBMEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDeEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDbEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDekYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw0QkFBNEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDM0YsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxxQkFBcUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDbkYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzVFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsaUNBQWlDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ2hHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsNEJBQTRCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzFGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsb0JBQW9CLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ2xGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQy9FLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ2xGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUM1RSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNuRixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDN0UsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDaEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw0QkFBNEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw0QkFBNEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDL0UsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDdkYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbkYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw4QkFBOEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDN0YsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx1QkFBdUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDdEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDakYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwwQkFBMEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDeEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw0QkFBNEIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDM0YsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxxQkFBcUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDbkYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxlQUFlLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzdFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQy9FLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsdUJBQXVCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3JGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsd0JBQXdCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3RGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsd0JBQXdCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ3ZGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsNEJBQTRCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQzNGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUN2RSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDdEUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw2QkFBNkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDNUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx1QkFBdUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDdEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxxQkFBcUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDcEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDcEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSwyQkFBMkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDMUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDakYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDeEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx1QkFBdUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDdEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbkYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDbEYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3pFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsNkJBQTZCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzNGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ25GLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsbUJBQW1CLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ2pGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ2hGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ25GLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzlFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsc0JBQXNCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3BGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsaUNBQWlDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQy9GLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsbUNBQW1DLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ2pHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsdUJBQXVCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQ3RGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFO0lBQzFGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUseUJBQXlCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3ZGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsZ0NBQWdDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzlGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUN6RSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLHlCQUF5QixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUN4RixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDekUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw2QkFBNkIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDNUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxnQ0FBZ0MsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDL0YsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxlQUFlLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQzdFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsMkJBQTJCLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFO0lBQ3pGLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUM5RSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGtDQUFrQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNoRyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTtJQUNoRixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLDZCQUE2QixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRTtJQUM1RixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7SUFDNUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxnQ0FBZ0MsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDOUYsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUU7SUFDdkYsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSwrQkFBK0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUU7Q0FDbEcsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztBQ3RIRixnRUFBa0Isa0NBQWtDLFVBQVUsMkRBQTJELHFFQUFxRSxFQUFFLDBEQUEwRCxFQUFFLEVBQUUsd0VBQXdFLHFFQUFxRSxFQUFFLEVBQUUsOERBQThELHFFQUFxRSxFQUFFLEVBQUUsaUVBQWlFLGtFQUFrRSxFQUFFLGlFQUFpRSxFQUFFLEVBQUUsNkRBQTZELDBEQUEwRCxFQUFFLDZEQUE2RCxFQUFFLEVBQUUsNkRBQTZELDBEQUEwRCxFQUFFLDZEQUE2RCxFQUFFLEVBQUUsNkRBQTZELHFFQUFxRSxFQUFFLEVBQUUsMkRBQTJELHNEQUFzRCxFQUFFLEVBQUUsK0RBQStELHFEQUFxRCxFQUFFLEVBQUUsMEVBQTBFLHFFQUFxRSxFQUFFLHdEQUF3RCxFQUFFLEVBQUUsMkRBQTJELDBEQUEwRCxFQUFFLDZEQUE2RCxFQUFFLEVBQUUsMkRBQTJELDBEQUEwRCxFQUFFLDZEQUE2RCxFQUFFLEVBQUUseURBQXlELDREQUE0RCxFQUFFLHdCO0FBQzNuRSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiw4Q0FBOEM7QUFDekUsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ1BhO0FBQ2IsOENBQThDLGNBQWM7QUFDNUQsd0JBQXdCLG1CQUFPLENBQUMsMkJBQW1CO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBQTtBQUFBO0FBQWtEO0FBRTNDLFNBQVMsWUFBWSxDQUFDLElBQWU7SUFDeEMsSUFBTSxJQUFJLEdBQVMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUUvQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksOERBQWEsRUFBRSxDQUFDO0FBQzlDLENBQUM7QUFFTSxTQUFTLGtCQUFrQixDQUFDLElBQTJCO0lBQzFELElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO0lBQ3ZCLFdBQVcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQ3RCLFdBQVcsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO0FBQ2hDLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQkQsNEdBQThELFFBQVEsbUJBQU8sQ0FBQywwQkFBMEIsRUFBRSxFQUFFO0FBQzVHLDBFQUEwRSxRQUFRLG1CQUFPLENBQUMsMEJBQTBCLEVBQUUsRUFBRTtBQUN4SCx3RkFBd0YsUUFBUSxtQkFBTyxDQUFDLDBCQUEwQixFQUFFLEVBQUU7QUFDdEksNEZBQTRGLFFBQVEsbUJBQU8sQ0FBQywwQkFBMEIsRUFBRSxFQUFFOztBQUUxSSxpZ0JBQWlnQiwwQkFBMEIsbUlBQW1JLGdCQUFnQixzTkFBc04sYUFBYSwyQkFBMkIsdUJBQXVCLCtEQUErRCxzQkFBc0IseUxBQXlMLDRCQUE0Qix3UUFBd1EsU0FBUywrRUFBK0UsV0FBVywrSkFBK0osU0FBUywrVkFBK1YsU0FBUyx5U0FBeVMsV0FBVyxnSUFBZ0ksU0FBUyw4RkFBOEYsb0VBQW9FLCtEQUErRCxtQkFBbUIsb1dBQW9XLG9FQUFvRSx5REFBeUQsbUJBQW1CLGtPQUFrTyxVQUFVLGdFO0FBQ3ovRyxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwrQ0FBK0M7QUFDMUUsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ1phO0FBQ2IsOENBQThDLGNBQWM7QUFDNUQsbUJBQW1CLG1CQUFPLENBQUMsa0NBQWtDO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7Ozs7Ozs7O0FDVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXNFO0FBQ0c7QUFDWDtBQUNaO0FBRXlGO0FBRTFHO0FBRWpDLElBQU0sa0JBQWtCLEdBQUcsMEtBSzFCLENBQUM7QUFFRixJQUFJLHNCQUFzQixHQUFZLElBQUksQ0FBQztBQUUzQztJQUFtQyxpQ0FBVTtJQU16QztRQUFBLFlBQ0ksaUJBQU8sU0FTVjtRQWZxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3JDLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUkxQyxLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxzRkFBZSxFQUFPLENBQUM7UUFFNUMscURBQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDaEIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7O0lBQ1AsQ0FBQztJQUVELDJDQUFtQixHQUFuQixVQUFvQixJQUFJO1FBQ3BCLElBQU0sUUFBUSxHQUFnQixJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRU8sZ0RBQXdCLEdBQWhDLFVBQWlDLFFBQXFCO1FBQ2xELElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUN6QixRQUFRLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFDO1NBQzNDO2FBQU07WUFDSCxRQUFRLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7U0FDcEQ7SUFDTCxDQUFDO0lBRUQsMENBQWtCLEdBQWxCLFVBQW1CLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSztRQUNqQyxPQUFPLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUN0RCxDQUFDO0lBRUQsdUNBQWUsR0FBZixVQUFnQixJQUFJO1FBQ2hCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUIsSUFBTSxRQUFRLEdBQWdCLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xFLElBQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7UUFDdEMsSUFBSSxRQUFRLENBQUMsU0FBUyxFQUFFO1lBQ3BCLEtBQUssQ0FBQyxHQUFHLEdBQUcsMEJBQTBCLENBQUM7WUFDdkMsUUFBUSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7U0FDOUI7YUFBTTtZQUNILEtBQUssQ0FBQyxHQUFHLEdBQUcsd0JBQXdCLENBQUM7WUFDckMsUUFBUSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDN0I7UUFFRCxRQUFRLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQsOENBQXNCLEdBQXRCLFVBQXVCLElBQUk7UUFDdkIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFNLFFBQVEsR0FBZ0IsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFbEUsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDM0IsUUFBUSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQztZQUN2QyxLQUFLLENBQUMsR0FBRyxHQUFHLDBCQUEwQixDQUFDO1lBQ3ZDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7U0FDckM7YUFBTTtZQUNILFFBQVEsQ0FBQyxpQkFBaUIsR0FBRyxVQUFDLElBQUk7Z0JBQzlCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUMxQixDQUFDLENBQUM7WUFDRixLQUFLLENBQUMsR0FBRyxHQUFHLHdCQUF3QixDQUFDO1lBQ3JDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7U0FDcEM7SUFDTCxDQUFDO0lBRUQsb0NBQVksR0FBWixVQUFhLElBQUk7UUFDYixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQU0sUUFBUSxHQUFnQixLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVsRSxJQUFJLHNCQUFzQixFQUFFO1lBQ3hCLElBQU0sVUFBVSxHQUFHLElBQUksMkVBQWtCLEVBQUUsQ0FBQztZQUM1QywrREFBSyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzNDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLFFBQVEsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzlCLHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUMvQixJQUFJLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEMsS0FBSyxDQUFDLEdBQUcsR0FBRyw0QkFBNEIsQ0FBQztZQUN6QyxRQUFRLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQztTQUN4QzthQUFNO1lBQ0gsSUFBTSxZQUFZLEdBQUcsSUFBSSw2RUFBb0IsRUFBRSxDQUFDO1lBQ2hELFFBQVEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQzdCLHNCQUFzQixHQUFHLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEMsS0FBSyxDQUFDLEdBQUcsR0FBRywwQkFBMEIsQ0FBQztZQUN2QyxRQUFRLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQztTQUMxQztJQUNMLENBQUM7SUFFRCxnREFBd0IsR0FBeEIsVUFBeUIsSUFBdUI7UUFBaEQsaUJBT0M7UUFORyxxREFBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtZQUNoQixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzdCLFFBQVEsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7SUFoR3FCO1FBQXJCLHlGQUFrQixFQUFFOztpREFBd0I7SUFDdkI7UUFBckIseUZBQWtCLEVBQUU7c0RBQVksc0ZBQWUsb0JBQWYsc0ZBQWU7b0RBQU07SUFnRzFELG9CQUFDO0NBQUEsQ0FsR2tDLDJFQUFVLEdBa0c1QztBQWxHeUI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJiO0FBQ2IsOENBQThDLGNBQWM7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7Ozs7Ozs7OztBQ05BO0FBQUE7QUFBTyxTQUFTLGtCQUFrQjtJQUM5QixPQUFPLFVBQUMsTUFBa0IsRUFBRSxXQUFtQjtRQUMzQyxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUU7WUFDdkMsR0FBRztnQkFDQyxPQUFPLElBQUksQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDLENBQUM7WUFDbkMsQ0FBQztZQUNELEdBQUcsWUFBQyxLQUFLO2dCQUNMLElBQUksSUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsS0FBSyxLQUFLLEVBQUU7b0JBQ25DLE9BQU87aUJBQ1Y7Z0JBRUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDbEQsQ0FBQztZQUNELFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFlBQVksRUFBRSxJQUFJO1NBQ3JCLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQztBQUNOLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQ0QscUQ7Ozs7Ozs7QUNBQSx5RDs7Ozs7OztBQ0FBLGlFOzs7Ozs7O0FDQUEsNkQ7Ozs7Ozs7QUNBQSxtRTs7Ozs7OztBQ0FBLHlEOzs7Ozs7O0FDQUEsc0Q7Ozs7Ozs7QUNBQSxzRDs7Ozs7OztBQ0FBLCtEOzs7Ozs7O0FDQUEsb0UiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIG1hcCA9IHtcblx0XCIuL2FwcC5jc3NcIjogXCIuL2FwcC5jc3NcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0dmFyIGlkID0gbWFwW3JlcV07XG5cdGlmKCEoaWQgKyAxKSkgeyAvLyBjaGVjayBmb3IgbnVtYmVyIG9yIHN0cmluZ1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gaWQ7XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuLyBzeW5jIF5cXFxcLlxcXFwvYXBwXFxcXC4oY3NzfHNjc3N8bGVzc3xzYXNzKSRcIjsiLCJ2YXIgbWFwID0ge1xuXHRcIi4vYXBwLXJvb3QueG1sXCI6IFwiLi9hcHAtcm9vdC54bWxcIixcblx0XCIuL2FwcC5jc3NcIjogXCIuL2FwcC5jc3NcIixcblx0XCIuL2FwcC5qc1wiOiBcIi4vYXBwLmpzXCIsXG5cdFwiLi9hcHAudHNcIjogXCIuL2FwcC50c1wiLFxuXHRcIi4vaG9tZS9kYXRhLnRzXCI6IFwiLi9ob21lL2RhdGEudHNcIixcblx0XCIuL2hvbWUvaG9tZS1wYWdlLmNzc1wiOiBcIi4vaG9tZS9ob21lLXBhZ2UuY3NzXCIsXG5cdFwiLi9ob21lL2hvbWUtcGFnZS5qc1wiOiBcIi4vaG9tZS9ob21lLXBhZ2UuanNcIixcblx0XCIuL2hvbWUvaG9tZS1wYWdlLnRzXCI6IFwiLi9ob21lL2hvbWUtcGFnZS50c1wiLFxuXHRcIi4vaG9tZS9ob21lLXBhZ2UueG1sXCI6IFwiLi9ob21lL2hvbWUtcGFnZS54bWxcIixcblx0XCIuL2hvbWUvaG9tZS12aWV3LW1vZGVsLmpzXCI6IFwiLi9ob21lL2hvbWUtdmlldy1tb2RlbC5qc1wiLFxuXHRcIi4vaG9tZS9ob21lLXZpZXctbW9kZWwudHNcIjogXCIuL2hvbWUvaG9tZS12aWV3LW1vZGVsLnRzXCIsXG5cdFwiLi9vYnNlcnZhYmxlLXByb3BlcnR5LWRlY29yYXRvci5qc1wiOiBcIi4vb2JzZXJ2YWJsZS1wcm9wZXJ0eS1kZWNvcmF0b3IuanNcIixcblx0XCIuL29ic2VydmFibGUtcHJvcGVydHktZGVjb3JhdG9yLnRzXCI6IFwiLi9vYnNlcnZhYmxlLXByb3BlcnR5LWRlY29yYXRvci50c1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHR2YXIgaWQgPSBtYXBbcmVxXTtcblx0aWYoIShpZCArIDEpKSB7IC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBpZDtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vIHN5bmMgcmVjdXJzaXZlICg/PCFcXFxcYkFwcF9SZXNvdXJjZXNcXFxcYi4qKSg/PCFcXFxcLlxcXFwvXFxcXGJ0ZXN0c1xcXFxiXFxcXC8uKj8pXFxcXC4oeG1sfGNzc3xqc3xrdHwoPzwhXFxcXC5kXFxcXC4pdHN8KD88IVxcXFxiX1tcXFxcdy1dKlxcXFwuKXNjc3MpJFwiOyIsIlxubW9kdWxlLmV4cG9ydHMgPSBcIjxGcmFtZSBkZWZhdWx0UGFnZT1cXFwiaG9tZS9ob21lLXBhZ2VcXFwiPjwvRnJhbWU+XFxuXCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vYXBwLXJvb3QueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vYXBwLXJvb3QueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsImdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIn5AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIikpO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIiwgKCkgPT4gcmVxdWlyZShcIkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2NvcmUuY3NzXCIpKTtcbmdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIn5AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9kZWZhdWx0LmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcIikpO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcIiwgKCkgPT4gcmVxdWlyZShcIkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2RlZmF1bHQuY3NzXCIpKTttb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOlt7XCJ0eXBlXCI6XCJpbXBvcnRcIixcImltcG9ydFwiOlwiJ35AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzcydcIn0se1widHlwZVwiOlwiaW1wb3J0XCIsXCJpbXBvcnRcIjpcIid+QG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3MnXCJ9LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idG5cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5ob21lLXBhbmVsXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwidmVydGljYWwtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpblwiLFwidmFsdWVcIjpcIjE1XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmRlc2NyaXB0aW9uLWxhYmVsXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLWJvdHRvbVwiLFwidmFsdWVcIjpcIjE1XCJ9XX1dLFwicGFyc2luZ0Vycm9yc1wiOltdfX07OyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2FwcC5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL2FwcC5jc3NcIiB9KTtcbiAgICB9KTtcbn0gIiwiXCJ1c2Ugc3RyaWN0XCI7XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG52YXIgYXBwID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIik7XG5hcHAucnVuKHsgbW9kdWxlTmFtZTogJ2FwcC1yb290JyB9KTtcbi8qXG5EbyBub3QgcGxhY2UgYW55IGNvZGUgYWZ0ZXIgdGhlIGFwcGxpY2F0aW9uIGhhcyBiZWVuIHN0YXJ0ZWQgYXMgaXQgd2lsbCBub3RcbmJlIGV4ZWN1dGVkIG9uIGlPUy5cbiovXG4iLCJpbXBvcnQgKiBhcyBhcHAgZnJvbSAndG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbic7XG5hcHAucnVuKHsgbW9kdWxlTmFtZTogJ2FwcC1yb290JyB9KTtcbi8qXG5EbyBub3QgcGxhY2UgYW55IGNvZGUgYWZ0ZXIgdGhlIGFwcGxpY2F0aW9uIGhhcyBiZWVuIHN0YXJ0ZWQgYXMgaXQgd2lsbCBub3RcbmJlIGV4ZWN1dGVkIG9uIGlPUy5cbiovXG4iLCJleHBvcnQgZnVuY3Rpb24gZ2V0RGF0YSgpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgY29uc3QgZGF0YUNvcHkgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICg8YW55PmRhdGFDb3B5W2ldKS5mYXZvdXJpdGUgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHJhbmRvbUltYWdlSWQgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiA3KTtcclxuICAgICAgICAgICAgKDxhbnk+ZGF0YUNvcHlbaV0pLmltYWdlID0gXCJ+L2ltYWdlcy9ncm9jZXJ5XCIgKyByYW5kb21JbWFnZUlkICsgXCIuanBnXCI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgcmVzb2x2ZShkYXRhQ29weSk7XHJcbiAgICAgICAgfSwgMTAwMCk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuY29uc3QgZGF0YSA9IFtcclxuICAgIHsgXCJpZFwiOiAxLCBcInByb2R1Y3RcIjogXCJUYXJ0IFNoZWxscyAtIFN3ZWV0LCAyXCIsIFwicHJpY2VcIjogXCIkMi40NlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMiwgXCJwcm9kdWN0XCI6IFwiQ2hvY29sYXRlIEVjbGFpcnNcIiwgXCJwcmljZVwiOiBcIiQ4LjIwXCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAzLCBcInByb2R1Y3RcIjogXCJPaWwgLSBTaG9ydGVuaW5nIC0gQWxsIC0gUHVycG9zZVwiLCBcInByaWNlXCI6IFwiJDUuMjJcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDQsIFwicHJvZHVjdFwiOiBcIkxpZCAtIDEwLDEyLDE2IE96XCIsIFwicHJpY2VcIjogXCIkMi44MlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNSwgXCJwcm9kdWN0XCI6IFwiRHVjayAtIEZhdFwiLCBcInByaWNlXCI6IFwiJDcuNDdcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA2LCBcInByb2R1Y3RcIjogXCJDaG9jb2xhdGUgQmFyIC0gUmVlc2UgUGllY2VzXCIsIFwicHJpY2VcIjogXCIkNS44N1wiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDcsIFwicHJvZHVjdFwiOiBcIk11ZmZpbiBPcmFuZ2UgSW5kaXZpZHVhbFwiLCBcInByaWNlXCI6IFwiJDIuOTNcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDgsIFwicHJvZHVjdFwiOiBcIlBhc3RyeSAtIENoZXJyeSBEYW5pc2ggLSBNaW5pXCIsIFwicHJpY2VcIjogXCIkOS4zMlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogOSwgXCJwcm9kdWN0XCI6IFwiV2luZSAtIE1haXBvIFZhbGxlIENhYmVybmV0XCIsIFwicHJpY2VcIjogXCIkOC42MlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMTAsIFwicHJvZHVjdFwiOiBcIkp1aWNlIC0gT3JhbmdpbmFcIiwgXCJwcmljZVwiOiBcIiQyLjc0XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAxMSwgXCJwcm9kdWN0XCI6IFwiTWVsZGVhIEdyZWVuIFRlYSBMaXF1b3JcIiwgXCJwcmljZVwiOiBcIiQ5LjAxXCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogMTIsIFwicHJvZHVjdFwiOiBcIkJlZWYgRmxhdCBJcm9uIFN0ZWFrXCIsIFwicHJpY2VcIjogXCIkMi40MVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMTMsIFwicHJvZHVjdFwiOiBcIkZvYW0gRXNwcmVzc28gQ3VwIFBsYWluIFdoaXRlXCIsIFwicHJpY2VcIjogXCIkNi44MlwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDE0LCBcInByb2R1Y3RcIjogXCJZZWFzdCAtIEZyZXNoLCBGbGVpc2NobWFuXCIsIFwicHJpY2VcIjogXCIkNi4xOFwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDE1LCBcInByb2R1Y3RcIjogXCJPa3JhXCIsIFwicHJpY2VcIjogXCIkNi41NlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMTYsIFwicHJvZHVjdFwiOiBcIk95c3RlcnMgLSBTbW9rZWRcIiwgXCJwcmljZVwiOiBcIiQyLjU5XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogMTcsIFwicHJvZHVjdFwiOiBcIkNvZmZlZSBDYXJhbWVsIEJpc2NvdHRpXCIsIFwicHJpY2VcIjogXCIkMi4xNFwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMTgsIFwicHJvZHVjdFwiOiBcIlNhdm9yeVwiLCBcInByaWNlXCI6IFwiJDMuMTVcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiAxOSwgXCJwcm9kdWN0XCI6IFwiVmVhbCAtIEluc2lkZVwiLCBcInByaWNlXCI6IFwiJDQuNzVcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiAyMCwgXCJwcm9kdWN0XCI6IFwiV2luZSAtIFJlZCwgR2FsbG8sIE1lcmxvdFwiLCBcInByaWNlXCI6IFwiJDEuMjZcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDIxLCBcInByb2R1Y3RcIjogXCJPaWwgLSBNYWNhZGFtaWFcIiwgXCJwcmljZVwiOiBcIiQ4LjYxXCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAyMiwgXCJwcm9kdWN0XCI6IFwiU3ByaW5nIFJvbGwgV3JhcHBlcnNcIiwgXCJwcmljZVwiOiBcIiQwLjYyXCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogMjMsIFwicHJvZHVjdFwiOiBcIlNob3J0YnJlYWQgLSBDb29raWUgQ3J1bWJzXCIsIFwicHJpY2VcIjogXCIkNC4xMVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMjQsIFwicHJvZHVjdFwiOiBcIlB1ZmYgUGFzdHJ5IC0gU2hlZXRzXCIsIFwicHJpY2VcIjogXCIkNS41OFwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMjUsIFwicHJvZHVjdFwiOiBcIkFwcGV0aXplciAtIEFzc29ydGVkIEJveFwiLCBcInByaWNlXCI6IFwiJDYuOTBcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDI2LCBcInByb2R1Y3RcIjogXCJPbmlvbnMgLSBSZWQgUGVhcmxcIiwgXCJwcmljZVwiOiBcIiQ1LjU5XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAyNywgXCJwcm9kdWN0XCI6IFwiQ2hlZXNlIC0gQnJpY2sgV2l0aCBPbmlvblwiLCBcInByaWNlXCI6IFwiJDguMjFcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDI4LCBcInByb2R1Y3RcIjogXCJCZWVyIC0gU2xlZW1hbiBGaW5lIFBvcnRlclwiLCBcInByaWNlXCI6IFwiJDMuNjNcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiAyOSwgXCJwcm9kdWN0XCI6IFwiU3RvY2sgLSBCZWVmLCBXaGl0ZVwiLCBcInByaWNlXCI6IFwiJDkuMDZcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDMwLCBcInByb2R1Y3RcIjogXCJTYWx0IC0gVGFibGVcIiwgXCJwcmljZVwiOiBcIiQ2LjI5XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAzMSwgXCJwcm9kdWN0XCI6IFwiU3VnYXIgLSBTd2VldCBOIExvdywgSW5kaXZpZHVhbFwiLCBcInByaWNlXCI6IFwiJDkuMDhcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiAzMiwgXCJwcm9kdWN0XCI6IFwiV2luZSAtIEdlb3JnZSBEdWJvZXVmIFJvc2VcIiwgXCJwcmljZVwiOiBcIiQ1Ljg4XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAzMywgXCJwcm9kdWN0XCI6IFwiQnJlYWQgLSBGbGF0IEJyZWFkXCIsIFwicHJpY2VcIjogXCIkMS42NFwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMzQsIFwicHJvZHVjdFwiOiBcIkxhbWIgLSBTaG91bGRlclwiLCBcInByaWNlXCI6IFwiJDAuMjRcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDM1LCBcInByb2R1Y3RcIjogXCJWYWNjdW0gQmFnIDEweDEzXCIsIFwicHJpY2VcIjogXCIkMTAuMDBcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiAzNiwgXCJwcm9kdWN0XCI6IFwiTWlsayBQb3dkZXJcIiwgXCJwcmljZVwiOiBcIiQ5LjMwXCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogMzcsIFwicHJvZHVjdFwiOiBcIlRyb3V0IFJhaW5ib3cgV2hvbGVcIiwgXCJwcmljZVwiOiBcIiQxLjk0XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiAzOCwgXCJwcm9kdWN0XCI6IFwiSGFtIC0gQ29va2VkXCIsIFwicHJpY2VcIjogXCIkNC4xMVwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDM5LCBcInByb2R1Y3RcIjogXCJTdHJhd3MgLSBDb2NrdGFsZVwiLCBcInByaWNlXCI6IFwiJDYuNzVcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA0MCwgXCJwcm9kdWN0XCI6IFwiUm9zZW1hcnkgLSBGcmVzaFwiLCBcInByaWNlXCI6IFwiJDkuMDNcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDQxLCBcInByb2R1Y3RcIjogXCJMaWQgLSAxMCwxMiwxNiBPelwiLCBcInByaWNlXCI6IFwiJDYuNjBcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA0MiwgXCJwcm9kdWN0XCI6IFwiTXVmZmluIE1peCAtIE1vcm5pbmcgR2xvcnlcIiwgXCJwcmljZVwiOiBcIiQwLjMwXCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA0MywgXCJwcm9kdWN0XCI6IFwiRmlzaCAtIFNvdXAgQmFzZSwgQm91aWxsb25cIiwgXCJwcmljZVwiOiBcIiQxLjA0XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA0NCwgXCJwcm9kdWN0XCI6IFwiQmVlciAtIE1hdWRpdGVcIiwgXCJwcmljZVwiOiBcIiQ0LjU2XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogNDUsIFwicHJvZHVjdFwiOiBcIlBhcmFzb2wgUGljayBTdGlyIFN0aWNrXCIsIFwicHJpY2VcIjogXCIkMi40MlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNDYsIFwicHJvZHVjdFwiOiBcIlBpY2tlcmVsIC0gRmlsbGV0c1wiLCBcInByaWNlXCI6IFwiJDUuMjlcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA0NywgXCJwcm9kdWN0XCI6IFwiQmFnZWwgLSBFdmVyeXRoaW5nIFByZXNsaWNlZFwiLCBcInByaWNlXCI6IFwiJDkuMzVcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA0OCwgXCJwcm9kdWN0XCI6IFwiUG9yayAtIEJ1dHQsIEJvbmVsZXNzXCIsIFwicHJpY2VcIjogXCIkMS41MlwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDQ5LCBcInByb2R1Y3RcIjogXCJWZWN0b3IgRW5lcmd5IEJhclwiLCBcInByaWNlXCI6IFwiJDEuMjZcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA1MCwgXCJwcm9kdWN0XCI6IFwiVmVhbCAtIFN3ZWV0YnJlYWRcIiwgXCJwcmljZVwiOiBcIiQ3LjAwXCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA1MSwgXCJwcm9kdWN0XCI6IFwiTGFtYiAtIFdob2xlIEhlYWQgT2ZmLG56XCIsIFwicHJpY2VcIjogXCIkNy45OVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNTIsIFwicHJvZHVjdFwiOiBcIkFyY3RpYyBDaGFyIC0gRnJlc2gsIFdob2xlXCIsIFwicHJpY2VcIjogXCIkMS4wOVwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDUzLCBcInByb2R1Y3RcIjogXCJNdWZmaW4gSGluZ2UgLSAyMTFuXCIsIFwicHJpY2VcIjogXCIkOC4zMVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNTQsIFwicHJvZHVjdFwiOiBcIlBlYXJzIC0gQW5qb3VcIiwgXCJwcmljZVwiOiBcIiQ3LjIyXCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA1NSwgXCJwcm9kdWN0XCI6IFwiVGFtYXJpbmQgUGFzdGVcIiwgXCJwcmljZVwiOiBcIiQ5LjQ4XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogNTYsIFwicHJvZHVjdFwiOiBcIkNoZWVzZSAtIFN3aXNzIFNsaWNlZFwiLCBcInByaWNlXCI6IFwiJDIuNTJcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDU3LCBcInByb2R1Y3RcIjogXCJGaWIgTjkgLSBQcmFndWUgUG93ZGVyXCIsIFwicHJpY2VcIjogXCIkMi43MlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNTgsIFwicHJvZHVjdFwiOiBcIkN1cCAtIFBhcGVyIDEwb3ogOTI5NTlcIiwgXCJwcmljZVwiOiBcIiQzLjM2XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogNTksIFwicHJvZHVjdFwiOiBcIldpbmUgLSBMYW1hbmNoYSBEbyBDcmlhbnphXCIsIFwicHJpY2VcIjogXCIkNy4xMFwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDYwLCBcInByb2R1Y3RcIjogXCJJc29tYWx0XCIsIFwicHJpY2VcIjogXCIkNi43NlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNjEsIFwicHJvZHVjdFwiOiBcIkhhZ2dpc1wiLCBcInByaWNlXCI6IFwiJDIuOTFcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDYyLCBcInByb2R1Y3RcIjogXCJJY2VjcmVhbSAtIERzdGsgU3RydyBDaHNlY2tcIiwgXCJwcmljZVwiOiBcIiQ1LjE1XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogNjMsIFwicHJvZHVjdFwiOiBcIkJyZWFkIC0gV2hpdGUsIFNsaWNlZFwiLCBcInByaWNlXCI6IFwiJDkuNDdcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA2NCwgXCJwcm9kdWN0XCI6IFwiVGVhIC0gQmxhY2sgQ3VycmFudFwiLCBcInByaWNlXCI6IFwiJDUuMTZcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA2NSwgXCJwcm9kdWN0XCI6IFwiVHJ1ZmZsZSBDdXBzIC0gQnJvd25cIiwgXCJwcmljZVwiOiBcIiQ4Ljc3XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA2NiwgXCJwcm9kdWN0XCI6IFwiUGVwcGVyIC0gSnVsaWVubmUsIEZyb3plblwiLCBcInByaWNlXCI6IFwiJDkuNDBcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA2NywgXCJwcm9kdWN0XCI6IFwiRmxvd2VyIC0gUG90bXVtc1wiLCBcInByaWNlXCI6IFwiJDguNjhcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA2OCwgXCJwcm9kdWN0XCI6IFwiTGFtYiBMZWcgLSBCb25lIC0gSW4gTnpcIiwgXCJwcmljZVwiOiBcIiQwLjE0XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogNjksIFwicHJvZHVjdFwiOiBcIktlbGxvZ3MgQWxsIEJyYW4gQmFyc1wiLCBcInByaWNlXCI6IFwiJDkuNDJcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA3MCwgXCJwcm9kdWN0XCI6IFwiQ2hlZXNlIC0gQ2FtYm96b2xhXCIsIFwicHJpY2VcIjogXCIkOC4zMFwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDcxLCBcInByb2R1Y3RcIjogXCJQZXBwZXIgLSBKYWxhcGVub1wiLCBcInByaWNlXCI6IFwiJDkuMzhcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA3MiwgXCJwcm9kdWN0XCI6IFwiR2x5Y2VyaW5lXCIsIFwicHJpY2VcIjogXCIkNy40N1wiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNzMsIFwicHJvZHVjdFwiOiBcIk11ZmZpbiBNaXggLSBDaG9jb2xhdGUgQ2hpcFwiLCBcInByaWNlXCI6IFwiJDIuOTNcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDc0LCBcInByb2R1Y3RcIjogXCJKdWljZSAtIEFwcGxlIENpZGVyXCIsIFwicHJpY2VcIjogXCIkOS40NFwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNzUsIFwicHJvZHVjdFwiOiBcIkFwcGxlIC0gTWFjaW50b3NoXCIsIFwicHJpY2VcIjogXCIkMS44NVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogNzYsIFwicHJvZHVjdFwiOiBcIlBydW5lcyAtIFBpdHRlZFwiLCBcInByaWNlXCI6IFwiJDYuOTZcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA3NywgXCJwcm9kdWN0XCI6IFwiUGluZWFwcGxlIC0gUmVndWxhclwiLCBcInByaWNlXCI6IFwiJDcuMzZcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDc4LCBcInByb2R1Y3RcIjogXCJDaGVlc2UgLSBTd2lzc1wiLCBcInByaWNlXCI6IFwiJDAuNTdcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDc5LCBcInByb2R1Y3RcIjogXCJMZXR0dWNlIC0gR3JlZW4gTGVhZlwiLCBcInByaWNlXCI6IFwiJDguMjZcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDgwLCBcInByb2R1Y3RcIjogXCJXaW5lIC0gRHVib3VlZiBNYWNvbiAtIFZpbGxhZ2VzXCIsIFwicHJpY2VcIjogXCIkOC40N1wiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogODEsIFwicHJvZHVjdFwiOiBcIlBhc3RhIC0gQ2FubmVsbG9uaSwgU2hlZXRzLCBGcmVzaFwiLCBcInByaWNlXCI6IFwiJDcuNjZcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDgyLCBcInByb2R1Y3RcIjogXCJDaGVlc2UgLSBTd2lzcyBTbGljZWRcIiwgXCJwcmljZVwiOiBcIiQ4LjQ5XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogODMsIFwicHJvZHVjdFwiOiBcIlNhbG1vbiBTdGVhayAtIENvaG9lIDggT3pcIiwgXCJwcmljZVwiOiBcIiQ4LjE0XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogODQsIFwicHJvZHVjdFwiOiBcIldpbmUgLSBUcmliYWwgU2F1dmlnbm9uXCIsIFwicHJpY2VcIjogXCIkNC45NlwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogODUsIFwicHJvZHVjdFwiOiBcIlR1cmtleSBMZWcgV2l0aCBEcnVtIEFuZCBUaGlnaFwiLCBcInByaWNlXCI6IFwiJDUuMzNcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDg2LCBcInByb2R1Y3RcIjogXCJTaGFsbG90c1wiLCBcInByaWNlXCI6IFwiJDguNzFcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA4NywgXCJwcm9kdWN0XCI6IFwiTXVzaHJvb20gLSBTaGl0YWtlLCBEcnlcIiwgXCJwcmljZVwiOiBcIiQ4LjU4XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogODgsIFwicHJvZHVjdFwiOiBcIlBpZSBQZWNhblwiLCBcInByaWNlXCI6IFwiJDQuNzJcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDg5LCBcInByb2R1Y3RcIjogXCJOb29kbGVzIC0gU3RlYW1lZCBDaG93IE1laW5cIiwgXCJwcmljZVwiOiBcIiQ2LjA1XCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogOTAsIFwicHJvZHVjdFwiOiBcIlRydWVibHVlIC0gQmx1ZWJlcnJ5IENyYW5iZXJyeVwiLCBcInByaWNlXCI6IFwiJDMuODVcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA5MSwgXCJwcm9kdWN0XCI6IFwiVHVybmlwIC0gTWluaVwiLCBcInByaWNlXCI6IFwiJDIuNjVcIiwgXCJhdmFpbGFibGVcIjogdHJ1ZSB9LFxyXG4gICAgeyBcImlkXCI6IDkyLCBcInByb2R1Y3RcIjogXCJWOCBTcGxhc2ggU3RyYXdiZXJyeSBLaXdpXCIsIFwicHJpY2VcIjogXCIkNy42NVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogOTMsIFwicHJvZHVjdFwiOiBcIlBlYXJzIC0gQW5qb3VcIiwgXCJwcmljZVwiOiBcIiQzLjEwXCIsIFwiYXZhaWxhYmxlXCI6IGZhbHNlIH0sXHJcbiAgICB7IFwiaWRcIjogOTQsIFwicHJvZHVjdFwiOiBcIldpbmUgLSBXaGl0ZSwgUmllc2xpbmcsIEhlbnJ5IE9mXCIsIFwicHJpY2VcIjogXCIkNS44OVwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogOTUsIFwicHJvZHVjdFwiOiBcIkJyZWFkIFd3IENsdXN0ZXJcIiwgXCJwcmljZVwiOiBcIiQ5LjM5XCIsIFwiYXZhaWxhYmxlXCI6IHRydWUgfSxcclxuICAgIHsgXCJpZFwiOiA5NiwgXCJwcm9kdWN0XCI6IFwiQnJlYWQgLSBJdGFsaWFuIFNlc2FtZSBQb2x5XCIsIFwicHJpY2VcIjogXCIkNi42MVwiLCBcImF2YWlsYWJsZVwiOiBmYWxzZSB9LFxyXG4gICAgeyBcImlkXCI6IDk3LCBcInByb2R1Y3RcIjogXCJEdWNrIC0gTGVnc1wiLCBcInByaWNlXCI6IFwiJDEuNTlcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfSxcclxuICAgIHsgXCJpZFwiOiA5OCwgXCJwcm9kdWN0XCI6IFwiRnJlbmNoIFBhc3RyeSAtIE1pbmkgQ2hvY29sYXRlXCIsIFwicHJpY2VcIjogXCIkNy44M1wiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogOTksIFwicHJvZHVjdFwiOiBcIk5hcGtpbiAtIEJldmVyYWdlIDEgUGx5XCIsIFwicHJpY2VcIjogXCIkMy40NFwiLCBcImF2YWlsYWJsZVwiOiB0cnVlIH0sXHJcbiAgICB7IFwiaWRcIjogMTAwLCBcInByb2R1Y3RcIjogXCJJc2xhbmQgT2FzaXMgLSBQZWFjaCBEYWlxdWlyaVwiLCBcInByaWNlXCI6IFwiJDEuMzFcIiwgXCJhdmFpbGFibGVcIjogZmFsc2UgfVxyXG5dO1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOlt7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuYWN0aW9uLWJhclwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjMEU0Mzc1XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCIjRjVDODUxXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxpc3QtZ3JvdXBcIixcIi5saXN0LWl0ZW1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI0ZGRkZGRlwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5oZWFkZXItbGluZWFyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiM1QkJDOTNcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubGlzdC1pdGVtLWxpbmVhclwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1ib3R0b20td2lkdGhcIixcInZhbHVlXCI6XCIxXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiIzBFNDM3NVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi50aXRsZS1saW5lYXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIiMwRTQzNzVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJwYWRkaW5nXCIsXCJ2YWx1ZVwiOlwiMCAwIDAgMTBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucHJpY2UtbGluZWFyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCIjRUY4MjNGXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwicGFkZGluZ1wiLFwidmFsdWVcIjpcIjAgMCAwIDEwXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnN3aXBlLWxpbmVhclwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjRUY4MjNGXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmZhdi1saW5lYXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW5cIixcInZhbHVlXCI6XCIzMFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5saXN0LWl0ZW0tZ3JpZFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpblwiLFwidmFsdWVcIjpcIjFcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubGlzdC1pdGVtLWdyaWQtYmFja2dyb3VuZFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjMDAwMDAwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwib3BhY2l0eVwiLFwidmFsdWVcIjpcIjAuNlwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi50aXRsZS1ncmlkXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCIjRkZGRkZGXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwicGFkZGluZ1wiLFwidmFsdWVcIjpcIjAgMCAwIDEwXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnByaWNlLWdyaWRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIiNFRjgyM0ZcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJwYWRkaW5nXCIsXCJ2YWx1ZVwiOlwiMCAwIDUgMTBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuZmF2LWdyaWRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW5cIixcInZhbHVlXCI6XCIwIDEwIDUgMFwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9ob21lL2hvbWUtcGFnZS5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL2hvbWUvaG9tZS1wYWdlLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJcInVzZSBzdHJpY3RcIjtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbnZhciBob21lX3ZpZXdfbW9kZWxfMSA9IHJlcXVpcmUoXCIuL2hvbWUtdmlldy1tb2RlbFwiKTtcbmZ1bmN0aW9uIHBhZ2VMb2FkZWQoYXJncykge1xuICAgIHZhciBwYWdlID0gYXJncy5vYmplY3Q7XG4gICAgcGFnZS5iaW5kaW5nQ29udGV4dCA9IG5ldyBob21lX3ZpZXdfbW9kZWxfMS5Ib21lVmlld01vZGVsKCk7XG59XG5leHBvcnRzLnBhZ2VMb2FkZWQgPSBwYWdlTG9hZGVkO1xuIiwiaW1wb3J0IHsgRXZlbnREYXRhIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCI7XG5pbXBvcnQgeyBQYWdlLCBWaWV3IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xuaW1wb3J0IHsgU3dpcGVBY3Rpb25zRXZlbnREYXRhIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiO1xuXG5pbXBvcnQgeyBIb21lVmlld01vZGVsIH0gZnJvbSBcIi4vaG9tZS12aWV3LW1vZGVsXCI7XG5cbmV4cG9ydCBmdW5jdGlvbiBuYXZpZ2F0aW5nVG8oYXJnczogRXZlbnREYXRhKSB7XG4gICAgY29uc3QgcGFnZSA9IDxQYWdlPmFyZ3Mub2JqZWN0O1xuXG4gICAgcGFnZS5iaW5kaW5nQ29udGV4dCA9IG5ldyBIb21lVmlld01vZGVsKCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBvblN3aXBlQ2VsbFN0YXJ0ZWQoYXJnczogU3dpcGVBY3Rpb25zRXZlbnREYXRhKSB7XG4gICAgY29uc3Qgc3dpcGVMaW1pdHMgPSBhcmdzLmRhdGEuc3dpcGVMaW1pdHM7XG4gICAgc3dpcGVMaW1pdHMubGVmdCA9IDM2MDtcbiAgICBzd2lwZUxpbWl0cy5yaWdodCA9IDA7XG4gICAgc3dpcGVMaW1pdHMudGhyZXNob2xkID0gMjAwO1xufVxuIiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXdcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcbmdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlldy9SYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGVzXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcuaXRlbVN3aXBlVGVtcGxhdGVcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcblxubW9kdWxlLmV4cG9ydHMgPSBcIjxQYWdlIG5hdmlnYXRpbmdUbz1cXFwibmF2aWdhdGluZ1RvXFxcIiBjbGFzcz1cXFwicGFnZVxcXCIgeG1sbnM9XFxcImh0dHA6Ly93d3cubmF0aXZlc2NyaXB0Lm9yZy90bnMueHNkXFxcIlxcbiAgICB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIj5cXG5cXG4gICAgPEFjdGlvbkJhciBjbGFzcz1cXFwiYWN0aW9uLWJhclxcXCI+XFxuICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwiYWN0aW9uLWJhci10aXRsZVxcXCIgd2lkdGg9XFxcIjEwMCVcXFwiPlxcbiAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJTdG9yZWZyb250XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgIDxTdGFja0xheW91dCBvcmllbnRhdGlvbj1cXFwiaG9yaXpvbnRhbFxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwicmlnaHRcXFwiXFxuICAgICAgICAgICAgICAgIG1hcmdpbj1cXFwiMCAxNSAwIDE1XFxcIj5cXG4gICAgICAgICAgICAgICAgPEltYWdlIHNyYz1cXFwifi9pbWFnZXMvZmF2LW91dGxpbmUucG5nXFxcIiBoZWlnaHQ9XFxcIjMwXFxcIiB0YXA9XFxcInt7IHRvZ2dsZUZhdm91cml0ZXNGaWx0ZXIgfX1cXFwiXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW49XFxcIjAgNSAwIDVcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcIn4vaW1hZ2VzL2xheW91dC1ncmlkLnBuZ1xcXCIgaGVpZ2h0PVxcXCIzMFxcXCIgdGFwPVxcXCJ7eyBjaGFuZ2VMYXlvdXQgfX1cXFwiXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW49XFxcIjAgNSAwIDVcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgPC9BY3Rpb25CYXI+XFxuXFxuICAgIDxHcmlkTGF5b3V0PlxcbiAgICAgICAgPGx2OlJhZExpc3RWaWV3IGlkPVxcXCJsaXN0LXZpZXdcXFwiIGNsYXNzPVxcXCJsaXN0LWdyb3VwXFxcIiBpdGVtcz1cXFwie3sgZGF0YUl0ZW1zIH19XFxcIlxcbiAgICAgICAgICAgIGxvYWRlZD1cXFwie3sgb25SYWRMaXN0Vmlld0xvYWRlZCB9fVxcXCIgc3dpcGVBY3Rpb25zPVxcXCJ0cnVlXFxcIlxcbiAgICAgICAgICAgIGl0ZW1UZW1wbGF0ZVNlbGVjdG9yPVxcXCJ7eyBzZWxlY3RJdGVtVGVtcGxhdGUgfX1cXFwiIHNlcGFyYXRvckNvbG9yPVxcXCJ0cmFuc3BhcmVudFxcXCJcXG4gICAgICAgICAgICBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiTm9uZVxcXCIgaXRlbVN3aXBlUHJvZ3Jlc3NTdGFydGVkPVxcXCJvblN3aXBlQ2VsbFN0YXJ0ZWRcXFwiXFxuICAgICAgICAgICAgcHVsbFRvUmVmcmVzaD1cXFwidHJ1ZVxcXCIgcHVsbFRvUmVmcmVzaEluaXRpYXRlZD1cXFwie3sgb25QdWxsVG9SZWZyZXNoSW5pdGlhdGVkIH19XFxcIj5cXG5cXG4gICAgICAgICAgICA8bHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlcz5cXG4gICAgICAgICAgICAgICAgPHRlbXBsYXRlIGtleT1cXFwibGluZWFyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIjEwMFxcXCIgY29sdW1ucz1cXFwiMTIwLCAqLCA4MFxcXCIgY2xhc3M9XFxcImxpc3QtaXRlbSBsaXN0LWl0ZW0tbGluZWFyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY29sPVxcXCIwXFxcIiByb3dTcGFuPVxcXCIyXFxcIiBzcmM9XFxcInt7IGltYWdlIH19XFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaXRcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMVxcXCIgdGV4dD1cXFwie3sgcHJvZHVjdCB9fVxcXCIgY2xhc3M9XFxcImgzIHRpdGxlLWxpbmVhclxcXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dFdyYXA9XFxcInRydWVcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJtaWRkbGVcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMlxcXCIgdGV4dD1cXFwie3sgcHJpY2UgfX1cXFwiIGNsYXNzPVxcXCJoMiBwcmljZS1saW5lYXJcXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHRXcmFwPVxcXCJ0cnVlXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwibWlkZGxlXFxcIiAvPlxcbiAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICA8L3RlbXBsYXRlPlxcblxcbiAgICAgICAgICAgICAgICA8dGVtcGxhdGUga2V5PVxcXCJncmlkXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIjEwMCwgNjBcXFwiIGNsYXNzPVxcXCJsaXN0LWl0ZW0gbGlzdC1pdGVtLWdyaWRcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSByb3c9XFxcIjBcXFwiIHNyYz1cXFwie3sgaW1hZ2UgfX1cXFwiIHN0cmV0Y2g9XFxcImFzcGVjdEZpbGxcXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd1NwYW49XFxcIjNcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93PVxcXCIxXFxcIiByb3dzPVxcXCIyNSwgMzVcXFwiIGNvbHVtbnM9XFxcIiosICpcXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJsaXN0LWl0ZW0tZ3JpZC1iYWNrZ3JvdW5kXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiBjb2xTcGFuPVxcXCIyXFxcIiB0ZXh0PVxcXCJ7eyBwcm9kdWN0IH19XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XFxcImgzIHRpdGxlLWdyaWRcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCByb3c9XFxcIjFcXFwiIGNvbD1cXFwiMFxcXCIgdGV4dD1cXFwie3sgcHJpY2UgfX1cXFwiIGNsYXNzPVxcXCJoMiBwcmljZS1ncmlkXFxcIiAvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2Ugcm93PVxcXCIxXFxcIiBjb2w9XFxcIjFcXFwiIHNyYz1cXFwie3sgZmF2b3VyaXRlID8gJ34vaW1hZ2VzL2Zhdi1zb2xpZC5wbmcnIDogJ34vaW1hZ2VzL2Zhdi1vdXRsaW5lLnBuZyd9fVxcXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJmYXYtZ3JpZFxcXCIgdGFwPVxcXCJ7eyB0b2dnbGVGYXZvdXJpdGUgfX1cXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCIgLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvdGVtcGxhdGU+XFxuICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGVzPlxcblxcbiAgICAgICAgICAgIDxsdjpSYWRMaXN0Vmlldy5pdGVtU3dpcGVUZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgY2xhc3M9XFxcInN3aXBlLWxpbmVhclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJ7eyBmYXZvdXJpdGUgPyAnfi9pbWFnZXMvZmF2LXNvbGlkLnBuZycgOiAnfi9pbWFnZXMvZmF2LW91dGxpbmUucG5nJ319XFxcIlxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVxcXCJmYXYtbGluZWFyXFxcIiB0YXA9XFxcInt7IHRvZ2dsZUZhdm91cml0ZSB9fVxcXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJsZWZ0XFxcIiBtYXJnaW49XFxcIjMwXFxcIj48L0ltYWdlPlxcbiAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtU3dpcGVUZW1wbGF0ZT5cXG4gICAgICAgIDwvbHY6UmFkTGlzdFZpZXc+XFxuXFxuICAgICAgICA8QWN0aXZpdHlJbmRpY2F0b3IgYnVzeT1cXFwie3sgaXNCdXN5IH19XFxcIiBjbGFzcz1cXFwiYWN0aXZpdHktaW5kaWNhdG9yXFxcIiAvPlxcbiAgICA8L0dyaWRMYXlvdXQ+XFxuPC9QYWdlPlwiOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2hvbWUvaG9tZS1wYWdlLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL2hvbWUvaG9tZS1wYWdlLnhtbFwiIH0pO1xuICAgIH0pO1xufSAiLCJcInVzZSBzdHJpY3RcIjtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcbnZhciBvYnNlcnZhYmxlXzEgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7XG52YXIgSG9tZVZpZXdNb2RlbCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uIChfc3VwZXIpIHtcbiAgICBfX2V4dGVuZHMoSG9tZVZpZXdNb2RlbCwgX3N1cGVyKTtcbiAgICBmdW5jdGlvbiBIb21lVmlld01vZGVsKCkge1xuICAgICAgICByZXR1cm4gX3N1cGVyLmNhbGwodGhpcykgfHwgdGhpcztcbiAgICB9XG4gICAgcmV0dXJuIEhvbWVWaWV3TW9kZWw7XG59KG9ic2VydmFibGVfMS5PYnNlcnZhYmxlKSk7XG5leHBvcnRzLkhvbWVWaWV3TW9kZWwgPSBIb21lVmlld01vZGVsO1xuIiwiaW1wb3J0IHsgT2JzZXJ2YWJsZVByb3BlcnR5IH0gZnJvbSBcIi4uL29ic2VydmFibGUtcHJvcGVydHktZGVjb3JhdG9yXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlQXJyYXkgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUtYXJyYXlcIjtcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIjtcbmltcG9ydCB7IGlzSU9TIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIjtcbmltcG9ydCB7IHBhcnNlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvYnVpbGRlclwiO1xuaW1wb3J0IHsgU3dpcGVBY3Rpb25zRXZlbnREYXRhLCBMaXN0Vmlld0V2ZW50RGF0YSwgUmFkTGlzdFZpZXcsIExpc3RWaWV3R3JpZExheW91dCwgTGlzdFZpZXdMaW5lYXJMYXlvdXQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCI7XG5cbmltcG9ydCB7IGdldERhdGEgfSBmcm9tIFwiLi9kYXRhXCI7XG5cbmNvbnN0IGhlYWRlckl0ZW1UZW1wbGF0ZSA9IGBcbiAgICA8R3JpZExheW91dCBjbGFzcz1cImhlYWRlci1saW5lYXJcIj5cbiAgICAgICAgPExhYmVsIHRleHQ9XCJTd2lwZSBSaWdodCB0byBBZGQgdG8gRmF2b3VyaXRlc1wiXG4gICAgICAgICAgICBob3Jpem9udGFsQWxpZ25tZW50PVwiY2VudGVyXCIgLz5cbiAgICA8L0dyaWRMYXlvdXQ+XG5gO1xuXG5sZXQgaXNMaXN0Vmlld0xpbmVhckxheW91dDogYm9vbGVhbiA9IHRydWU7XG5cbmV4cG9ydCBjbGFzcyBIb21lVmlld01vZGVsIGV4dGVuZHMgT2JzZXJ2YWJsZSB7XG4gICAgQE9ic2VydmFibGVQcm9wZXJ0eSgpIGlzQnVzeTogYm9vbGVhbiA9IHRydWU7XG4gICAgQE9ic2VydmFibGVQcm9wZXJ0eSgpIGRhdGFJdGVtczogT2JzZXJ2YWJsZUFycmF5PGFueT47XG5cbiAgICBwcml2YXRlIHNob3dGYXZvdXJpdGVzRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgdGhpcy5pc0J1c3kgPSB0cnVlO1xuICAgICAgICB0aGlzLnNob3dGYXZvdXJpdGVzRmlsdGVyID0gZmFsc2U7XG4gICAgICAgIHRoaXMuZGF0YUl0ZW1zID0gbmV3IE9ic2VydmFibGVBcnJheTxhbnk+KCk7XG5cbiAgICAgICAgZ2V0RGF0YSgpLnRoZW4oKGRhdGEpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZGF0YUl0ZW1zLnB1c2goZGF0YSk7XG4gICAgICAgICAgICB0aGlzLmlzQnVzeSA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBvblJhZExpc3RWaWV3TG9hZGVkKGFyZ3MpIHtcbiAgICAgICAgY29uc3QgbGlzdFZpZXc6IFJhZExpc3RWaWV3ID0gYXJncy5vYmplY3Q7XG4gICAgICAgIHRoaXMudG9nZ2xlSGVhZGVySXRlbVRlbXBsYXRlKGxpc3RWaWV3KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHRvZ2dsZUhlYWRlckl0ZW1UZW1wbGF0ZShsaXN0VmlldzogUmFkTGlzdFZpZXcpIHtcbiAgICAgICAgaWYgKCFpc0xpc3RWaWV3TGluZWFyTGF5b3V0KSB7XG4gICAgICAgICAgICBsaXN0Vmlldy5oZWFkZXJJdGVtVGVtcGxhdGUgPSB1bmRlZmluZWQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsaXN0Vmlldy5oZWFkZXJJdGVtVGVtcGxhdGUgPSBoZWFkZXJJdGVtVGVtcGxhdGU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzZWxlY3RJdGVtVGVtcGxhdGUoaXRlbSwgaW5kZXgsIGl0ZW1zKSB7XG4gICAgICAgIHJldHVybiBpc0xpc3RWaWV3TGluZWFyTGF5b3V0ID8gXCJsaW5lYXJcIiA6IFwiZ3JpZFwiO1xuICAgIH1cblxuICAgIHRvZ2dsZUZhdm91cml0ZShhcmdzKSB7XG4gICAgICAgIGNvbnN0IGltYWdlID0gYXJncy5vYmplY3Q7XG4gICAgICAgIGNvbnN0IGxpc3RWaWV3ID0gPFJhZExpc3RWaWV3PmltYWdlLnBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0LXZpZXdcIik7XG4gICAgICAgIGNvbnN0IGl0ZW1EYXRhID0gaW1hZ2UuYmluZGluZ0NvbnRleHQ7XG4gICAgICAgIGlmIChpdGVtRGF0YS5mYXZvdXJpdGUpIHtcbiAgICAgICAgICAgIGltYWdlLnNyYyA9IFwifi9pbWFnZXMvZmF2LW91dGxpbmUucG5nXCI7XG4gICAgICAgICAgICBpdGVtRGF0YS5mYXZvdXJpdGUgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGltYWdlLnNyYyA9IFwifi9pbWFnZXMvZmF2LXNvbGlkLnBuZ1wiO1xuICAgICAgICAgICAgaXRlbURhdGEuZmF2b3VyaXRlID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxpc3RWaWV3Lm5vdGlmeVN3aXBlVG9FeGVjdXRlRmluaXNoZWQoKTtcbiAgICB9XG5cbiAgICB0b2dnbGVGYXZvdXJpdGVzRmlsdGVyKGFyZ3MpIHtcbiAgICAgICAgY29uc3QgaW1hZ2UgPSBhcmdzLm9iamVjdDtcbiAgICAgICAgY29uc3QgbGlzdFZpZXcgPSA8UmFkTGlzdFZpZXc+aW1hZ2UucGFnZS5nZXRWaWV3QnlJZChcImxpc3Qtdmlld1wiKTtcblxuICAgICAgICBpZiAodGhpcy5zaG93RmF2b3VyaXRlc0ZpbHRlcikge1xuICAgICAgICAgICAgbGlzdFZpZXcuZmlsdGVyaW5nRnVuY3Rpb24gPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICBpbWFnZS5zcmMgPSBcIn4vaW1hZ2VzL2Zhdi1vdXRsaW5lLnBuZ1wiO1xuICAgICAgICAgICAgdGhpcy5zaG93RmF2b3VyaXRlc0ZpbHRlciA9IGZhbHNlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGlzdFZpZXcuZmlsdGVyaW5nRnVuY3Rpb24gPSAoaXRlbSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBpdGVtLmZhdm91cml0ZTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBpbWFnZS5zcmMgPSBcIn4vaW1hZ2VzL2Zhdi1zb2xpZC5wbmdcIjtcbiAgICAgICAgICAgIHRoaXMuc2hvd0Zhdm91cml0ZXNGaWx0ZXIgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgY2hhbmdlTGF5b3V0KGFyZ3MpIHtcbiAgICAgICAgY29uc3QgaW1hZ2UgPSBhcmdzLm9iamVjdDtcbiAgICAgICAgY29uc3QgbGlzdFZpZXcgPSA8UmFkTGlzdFZpZXc+aW1hZ2UucGFnZS5nZXRWaWV3QnlJZChcImxpc3Qtdmlld1wiKTtcblxuICAgICAgICBpZiAoaXNMaXN0Vmlld0xpbmVhckxheW91dCkge1xuICAgICAgICAgICAgY29uc3QgZ3JpZExheW91dCA9IG5ldyBMaXN0Vmlld0dyaWRMYXlvdXQoKTtcbiAgICAgICAgICAgIGlzSU9TID8gZ3JpZExheW91dC5pdGVtSGVpZ2h0ID0gMTYwIDogbnVsbDtcbiAgICAgICAgICAgIGdyaWRMYXlvdXQuc3BhbkNvdW50ID0gMjtcbiAgICAgICAgICAgIGxpc3RWaWV3LnN3aXBlQWN0aW9ucyA9IGZhbHNlO1xuICAgICAgICAgICAgaXNMaXN0Vmlld0xpbmVhckxheW91dCA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy50b2dnbGVIZWFkZXJJdGVtVGVtcGxhdGUobGlzdFZpZXcpO1xuICAgICAgICAgICAgaW1hZ2Uuc3JjID0gXCJ+L2ltYWdlcy9sYXlvdXQtbGluZWFyLnBuZ1wiO1xuICAgICAgICAgICAgbGlzdFZpZXcubGlzdFZpZXdMYXlvdXQgPSBncmlkTGF5b3V0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc3QgbGluZWFyTGF5b3V0ID0gbmV3IExpc3RWaWV3TGluZWFyTGF5b3V0KCk7XG4gICAgICAgICAgICBsaXN0Vmlldy5zd2lwZUFjdGlvbnMgPSB0cnVlO1xuICAgICAgICAgICAgaXNMaXN0Vmlld0xpbmVhckxheW91dCA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLnRvZ2dsZUhlYWRlckl0ZW1UZW1wbGF0ZShsaXN0Vmlldyk7XG4gICAgICAgICAgICBpbWFnZS5zcmMgPSBcIn4vaW1hZ2VzL2xheW91dC1ncmlkLnBuZ1wiO1xuICAgICAgICAgICAgbGlzdFZpZXcubGlzdFZpZXdMYXlvdXQgPSBsaW5lYXJMYXlvdXQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQoYXJnczogTGlzdFZpZXdFdmVudERhdGEpIHtcbiAgICAgICAgZ2V0RGF0YSgpLnRoZW4oKGRhdGEpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZGF0YUl0ZW1zLnNwbGljZSgwKTtcbiAgICAgICAgICAgIHRoaXMuZGF0YUl0ZW1zLnB1c2goZGF0YSk7XG4gICAgICAgICAgICBjb25zdCBsaXN0VmlldyA9IGFyZ3Mub2JqZWN0O1xuICAgICAgICAgICAgbGlzdFZpZXcubm90aWZ5UHVsbFRvUmVmcmVzaEZpbmlzaGVkKCk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiIsIlwidXNlIHN0cmljdFwiO1xuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xuZnVuY3Rpb24gT2JzZXJ2YWJsZVByb3BlcnR5KCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eUtleSkge1xuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBwcm9wZXJ0eUtleSwge1xuICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXNbXCJfXCIgKyBwcm9wZXJ0eUtleV07XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc2V0OiBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpc1tcIl9cIiArIHByb3BlcnR5S2V5XSA9PT0gdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzW1wiX1wiICsgcHJvcGVydHlLZXldID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZnlQcm9wZXJ0eUNoYW5nZShwcm9wZXJ0eUtleSwgdmFsdWUpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgICAgICAgICBjb25maWd1cmFibGU6IHRydWVcbiAgICAgICAgfSk7XG4gICAgfTtcbn1cbmV4cG9ydHMuT2JzZXJ2YWJsZVByb3BlcnR5ID0gT2JzZXJ2YWJsZVByb3BlcnR5O1xuIiwiLyoqXG5UaGlzIGZpbGUgZGVmaW5lcyBhIGRlY29yYXRvciB5b3UgY2FuIHVzZSB0byBlbmFibGUgdHdvLXdheVxuYmluZGluZyBvbiB2aWV3LW1vZGVsIHByb3BlcnRpZXMuXG5cbkZvciBleGFtcGxlOlxuXG5pbXBvcnQgeyBPYnNlcnZhYmxlUHJvcGVydHkgfSBmcm9tIFwiLi4vb2JzZXJ2YWJsZS1wcm9wZXJ0eS1kZWNvcmF0b3JcIjtcblxuQE9ic2VydmFibGVQcm9wZXJ0eSgpIG15UHJvcGVydHk6IGJvb2xlYW4gPSB0cnVlO1xuXG5SZWFkIG1vcmUgYXQgaHR0cHM6Ly93d3cubmF0aXZlc2NyaXB0Lm9yZy9ibG9nL25hdGl2ZXNjcmlwdC1vYnNlcnZhYmxlLW1hZ2ljLXN0cmluZy1wcm9wZXJ0eS1uYW1lLWJlLWdvbmVcbioqL1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3Rucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlJztcblxuZXhwb3J0IGZ1bmN0aW9uIE9ic2VydmFibGVQcm9wZXJ0eSgpIHtcbiAgICByZXR1cm4gKHRhcmdldDogT2JzZXJ2YWJsZSwgcHJvcGVydHlLZXk6IHN0cmluZykgPT4ge1xuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBwcm9wZXJ0eUtleSwge1xuICAgICAgICAgICAgZ2V0KCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzW1wiX1wiICsgcHJvcGVydHlLZXldO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNldCh2YWx1ZSkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzW1wiX1wiICsgcHJvcGVydHlLZXldID09PSB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpc1tcIl9cIiArIHByb3BlcnR5S2V5XSA9IHZhbHVlO1xuICAgICAgICAgICAgICAgIHRoaXMubm90aWZ5UHJvcGVydHlDaGFuZ2UocHJvcGVydHlLZXksIHZhbHVlKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlXG4gICAgICAgIH0pO1xuICAgIH07XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2J1bmRsZS1lbnRyeS1wb2ludHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGUtYXJyYXlcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9maWxlLXN5c3RlbVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy91aS9mcmFtZS9hY3Rpdml0eVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3N0eWxpbmcvc3R5bGUtc2NvcGVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==